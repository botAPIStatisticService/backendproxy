FROM nginx:1.19.0-alpine

RUN rm -rf /etc/nginx/nginx.conf
RUN mkdir -p /data/nginx/cache

COPY nginx.conf /etc/nginx/

EXPOSE 5000


